const User = require('../db/models/userModel');

const getAll = async () => await User.find();
const count = async () => await User.collection.countDocuments();
const getOne = async id => await User.findById(id);
const getUserByMail = async mail => await User.findOne({ mail });

module.exports = {
  getAll,
  count,
  getOne,
  getUserByMail
};
