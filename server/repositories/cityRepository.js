const City = require('../db/models/cityModel');

const getAll = async () => await City.find();
const count = async () => await City.collection.countDocuments();
const getOne = async id => await City.findById(id);
const getCityByName = async name => await City.find({ name });

module.exports = {
  getAll,
  count,
  getOne,
  getCityByName,
};
