const Itinerary = require('../db/models/itineraryModel');

const getAll = async () => await Itinerary.find();
const count = async () => await Itinerary.collection.countDocuments();
const getOne = async id => await Itinerary.findById(id);
const getItineraryByCity = async cityId => await Itinerary.find({ cityId });
const deleteComment = async (id, commentId) => await Itinerary.updateOne({ _id: id }, {
  $pull: { comments: { _id: commentId } }
});

module.exports = {
  getAll,
  count,
  getOne,
  getItineraryByCity,
  deleteComment
};
