require('dotenv').config();
const express = require("express");
const app = express();

const port = process.env.PORT || 5000;

const cors = require("cors");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const db = require('./keys').mongoURI;
const mongoose = require('mongoose');

mongoose.connect(db, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
})
  .then(() => console.log("Conexión a MongoDB establecida"))
  .catch(err => console.error(err))

app.use('/api', require('./routes/cities'));

app.use('/api/user', require('./routes/users'));

app.use('/api/itineraries', require('./routes/itineraries'));

app.use('/api/checkuser', require('./routes/comments'));
app.use('/api/comments', require('./routes/comments'));
app.use('/api/comment', require('./routes/comments'));

module.exports = {
  port,
  app
}