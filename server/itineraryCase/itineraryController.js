const get = require('./getItineraries/getItineraries');
const create = require('./createItinerary/createItinerary');
const deletes = require('./deleteItinerary/deleteItinerary');
const edit = require('./editItinerary/editItinerary');

module.exports = {
  get,
  create,
  deletes,
  edit
};