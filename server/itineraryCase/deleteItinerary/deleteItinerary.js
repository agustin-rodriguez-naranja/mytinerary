const { response } = require('../itineraryModule');
const itineraryRepository = require('../../repositories/itineraryRepository');

const deleteComment = async (req, res = response) => {
  try {
    const { id: userId } = req.user;
    const { id: commentId } = req.params;

    const itineraries = await itineraryRepository.getAll();

    let itineraryThatHasComment = null;

    for (let i = 0; i < itineraries.length; i++) {
      const match = itineraries[i].comments.find(comment => comment._id.toString() === commentId && comment.userId.toString() === userId);

      if (match) {
        itineraryThatHasComment = itineraries[i];
        break;
      }
    }

    if (!itineraryThatHasComment) {
      return res.status(404).json({
        ok: false,
        message: 'Selected comment was not found',
      });
    }

    await itineraryRepository.deleteComment(itineraryThatHasComment.id, commentId);

    const { comments: newCommentsArray } = await itineraryRepository.getOne(itineraryThatHasComment.id);

    res.status(200).json({
      response: newCommentsArray
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  deleteComment
}