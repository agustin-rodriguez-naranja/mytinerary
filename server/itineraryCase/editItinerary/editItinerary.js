const { response, Itinerary } = require('../itineraryModule');
const itineraryRepository = require('../../repositories/itineraryRepository');

const editComment = async (req, res = response) => {
  try {
    const { id: commentId } = req.params;
    const { id: userId } = req.user;
    const { text } = req.body;

    const itineraries = await itineraryRepository.getAll();

    let foundAndEdited = false;
    let newCommentsArray = [];

    for (let i = 0; i < itineraries.length; i++) {
      itineraries[i].comments.map(async comment => {
        if (comment._id.toString() === commentId) {
          comment.text = text;
          foundAndEdited = true;
          newCommentsArray = itineraries[i].comments;
          await itineraries[i].save();
        }

        return comment;
      });

      if (foundAndEdited) {
        break;
      }
    }

    if (!foundAndEdited) {
      return res.status(404).json({
        ok: false,
        message: 'Selected comment was not found',
      });
    }

    return res.status(200).json({
      response: newCommentsArray
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  editComment
};
