const { response } = require('../itineraryModule');
const itineraryRepository = require('../../repositories/itineraryRepository');

const createComment = async (req, res = response) => {
  try {
    const { id: userId, firstName, lastName, userPic } = req.user;
    const { id } = req.params;
    const { text } = req.body;

    const itinerary = await itineraryRepository.getOne(id);

    const newComment = {
      userId,
      text,
      userName: firstName + ' ' + lastName,
      userPic
    };

    itinerary.comments.push(newComment);

    await itinerary.save();

    const arrayUserComments = itinerary.comments.map(comment => {
      if (comment.userId.toString() === userId) {
        return comment._id;
      }
    })

    res.status(200).json({
      response: itinerary.comments,
      arrayOwnerCheck: arrayUserComments
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  createComment
}