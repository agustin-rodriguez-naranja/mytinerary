const { response } = require('../itineraryModule');
const itineraryRepository = require('../../repositories/itineraryRepository');

const getItineraryByCity = async (req, res = response) => {
  try {
    const cityId = req.params.cityId;
    const data = await itineraryRepository.getItineraryByCity(cityId);

    if (data.length === 0) {
      return res.status(401).json({
        ok: false,
        message: ''
      });
    }

    res.status(200).json({
      ok: true,
      response: data,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

const getCommentsFromUserInItinerary = async (req, res = response) => {
  try {
    const { id: userId } = req.user;
    const { id } = req.params;

    const itinerary = await itineraryRepository.getOne(id);

    if (!itinerary) {
      return res.status(404).json({
        ok: false,
        message: 'No se encontró el itinerario solicitado'
      });
    }

    const { comments, usersLike } = itinerary;

    const arrayOwnerCheck = comments.filter(comment => comment.userId.toString() === userId);
    const likedCheck = usersLike.includes(userId);

    return res.status(200).json({
      response: {
        arrayOwnerCheck,
        likedCheck
      }
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

const getItineraryLikes = async (req, res = response) => {
  try {
    const { id: userId } = req.user;
    const { id } = req.params;


    const { likes, usersLike } = await itineraryRepository.getOne(id);
    const liked = usersLike.includes(userId);

    res.status(200).json({
      response: {
        likes,
        liked
      }
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  getItineraryByCity,
  getCommentsFromUserInItinerary,
  getItineraryLikes
};