const request = require('supertest');
const { app } = require('../app');

let testServer;

beforeAll(() => {
  testServer = app.listen(4000);
});

afterAll((done) => {
  testServer = testServer.close(done);
});

describe('Get /api/user/signup', () => {
  it('Registers a user', async () => {
    const response = await request(app).post('/api/user/signup').send({
      firstName: 'Juan',
      lastName: 'Perez',
      email: 'juan@mail.com',
      password: 'juan123',
      country: 'Argentina',
      userPic: 'https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png'
    })
    expect(response.status).toBe(201)
    expect(response.body.ok).toBe(true)
    expect(response.body).not.toBeNull()
  });
});

describe('Get /api/user/signin', () => {
  it('Login user', async () => {
    const response = await request(app).post('/api/user/signin').send({
      email: 'juan@mail.com',
      password: 'juan123',
    })
    expect(response.status).toBe(200)
    expect(response.body.success).toBe(true)
    expect(response.body).not.toBeNull()
    expect(response.body.response.token.length !== 0).toBeTruthy()
    expect(response.body.response.firstName).toBe('Juan')
  });
});