const { response } = require('../cityModule');
const cityRepository = require('../../repositories/cityRepository');

const getCities = async (req, res = response) => {
  try {
    const data = await cityRepository.getAll();
    const count = await cityRepository.count();

    if (!data) {
      return res.status(401).json({
        ok: false,
        message: ''
      });
    }

    // res.status(200).json({
    //   ok: true,
    //   message: 'Ciudades',
    //   cities: data,
    //   total: count
    // });


    res.status(200).json({
      ok: true,
      response: data,
      total: count
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

const getCity = async (req, res = response) => {
  try {
    const id = req.params.id;

    const data = await cityRepository.getOne(id);

    if (!data) {
      return res.status(400).json({
        ok: false,
        message: '',
      });
    }

    res.status(200).json({
      ok: true,
      message: 'Ciudad',
      cities: data
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

const getCityByQuery = async (req, res = response) => {
  try {
    const name = req.query.name;

    const data = await cityRepository.getCityByName(name);

    if (data.length === 0) {
      return res.status(400).json({
        ok: false,
        message: '',
      });
    }

    return res.status(200).json({
      ok: true,
      message: 'Ciudad',
      cities: data
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  getCities,
  getCity,
  getCityByQuery
};