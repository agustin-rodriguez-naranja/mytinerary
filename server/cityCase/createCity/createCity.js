const { City, response } = require('../cityModule');
const cityRepository = require('../../repositories/cityRepository');

const create = async (req, res = response) => {
  try {
    const allCities = await cityRepository.getAll();

    const duplicated = allCities.some(city => city.name === req.body.name && city.country === req.body.country);

    if (duplicated) {
      return res.status(422).send("Ya existe una ciudad con el mismo nombre para el mismo país");
    }

    const newCity = new City({
      name: req.body.name,
      country: req.body.country,
      img: `https://lorempixel.com/1200/800/city?t=${Date.now()}`
    });

    const city = await newCity.save();

    res.status(201).json({
      ok: true,
      message: 'La ciudad se creó correctamente',
      city
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  create
};