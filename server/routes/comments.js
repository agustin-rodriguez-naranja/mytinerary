const { Router } = require('../itineraryCase/itineraryModule');
const { get, create, deletes, edit } = require('../itineraryCase/itineraryController');
const passport = require('passport');
const { fieldsValidator } = require('../middlewares/fields-validator');
const { check } = require('express-validator');

const router = new Router();

router.get('/:id', passport.authenticate('jwt', { session: false }), get.getCommentsFromUserInItinerary);

router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  [
    check('text', 'A text is mandatory').not().isEmpty(),
    fieldsValidator
  ],
  create.createComment
);

router.delete('/:id', passport.authenticate('jwt', { session: false }), deletes.deleteComment);

router.put('/:id', passport.authenticate('jwt', { session: false }), edit.editComment);

module.exports = router;