const { Router } = require('../userCase/userModule');
const { create, get } = require('../userCase/userController');
const { check } = require('express-validator');
const { fieldsValidator } = require('../middlewares/fields-validator');
const passport = require('passport');

const router = new Router();

router.post(
  '/signup',
  [
    check('lastName', 'Last Name field is mandatory').not().isEmpty(),
    check('firstName', 'First Name field is mandatory').not().isEmpty(),
    check('email', 'Email field is mandatory').isEmail(),
    check('password', 'Password field is mandatory').not().isEmpty().isLength({ min: 6, max: 16 }),
    check('country', 'Country field is mandatory').not().isEmpty(),
    check('userPic', 'Url Image field is mandatory').not().isEmpty(),
    fieldsValidator
  ],
  create.create
);

router.post('/signin', get.signin);

router.get('/signinls', passport.authenticate('jwt', { session: false }), get.signinls);

module.exports = router;