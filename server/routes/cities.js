const { Router } = require('../cityCase/cityModule');
const { create, get } = require('../cityCase/cityController');

const router = new Router();

router.get('/prueba', (req, res) => {
  res.send({
    msg: 'Ruta de prueba de ciudades'
  });
})

router.get('/cities', get.getCities);
router.get('/city/:id', get.getCity);
router.get('/city', get.getCityByQuery);

router.post('/', create.create);

module.exports = router;