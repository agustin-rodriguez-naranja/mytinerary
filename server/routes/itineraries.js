const { Router } = require('../itineraryCase/itineraryModule');
const { get } = require('../itineraryCase/itineraryController');
const passport = require('passport');

const router = new Router();

router.get('/:cityId', get.getItineraryByCity);
router.get('/like/:id', passport.authenticate('jwt', { session: false }), get.getItineraryLikes);

module.exports = router;