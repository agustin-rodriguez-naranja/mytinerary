const { response } = require('express');
const userRepository = require('../../repositories/userRepository');
const bcrypt = require('bcryptjs');
const key = require('../../keys');
const jwt = require('jsonwebtoken');

const signin = async (req, res = response) => {
  try {
    const { email: mail, password } = req.body;

    const foundUser = await userRepository.getUserByMail(mail);

    if (!foundUser) {
      return res.status(400).json({
        ok: false,
        message: 'Provided mail is not registered'
      });
    }

    const passwordIsValid = bcrypt.compareSync(password, foundUser.password)

    if (!passwordIsValid) {
      return res.json({
        ok: false,
        message: 'Invalid credentials'
      });
    }

    const payload = {
      id: foundUser.id,
      email: foundUser.mail,
      userPic: foundUser.userPic
    };

    const options = { expiresIn: 2592000 };

    jwt.sign(
      payload,
      key.secretOrKey,
      options,
      (err, token) => {
        if (err) {
          res.json({
            success: false,
            response: 'There was an error'
          });
        } else {
          res.json({
            success: true,
            response: {
              token,
              firstName: foundUser.firstName,
              userPic: foundUser.userPic
            }
          });
        }
      }
    );
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

const signinls = async (req, res = response) => {
  try {
    const { firstName, userPic } = req.user;

    res.status(200).json({
      response: {
        firstName,
        userPic
      }
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  signin,
  signinls
}