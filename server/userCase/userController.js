const create = require('./createUsers/createUsers');
const get = require('./getUsers/getUsers');

module.exports = {
  create,
  get
};