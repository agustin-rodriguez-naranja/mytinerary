const { User, response } = require('../userModule');
const userRepository = require('../../repositories/userRepository');
const bcrypt = require('bcryptjs');

const create = async (req, res = response) => {
  try {
    const { firstName, lastName, email: mail, password, userPic, country } = req.body;

    const mailAlreadyExists = await userRepository.getUserByMail(mail);

    if (mailAlreadyExists) {
      return res.status(400).json({
        ok: false,
        message: 'Provided mail is already in use',
      });
    }

    const salt = bcrypt.genSaltSync();
    const hashedPassword = bcrypt.hashSync(password, salt);

    const newUser = new User({
      firstName,
      lastName,
      mail,
      password: hashedPassword,
      userPic,
      country
    });

    await newUser.save();

    res.status(200).json({
      success: true,
      response: newUser
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message: 'Error interno del servidor',
      error
    });
  }
}

module.exports = {
  create
};